/*PeopleInterestingFields*/
{
	fieldName: "name",
	formatAction: null
},
{
	fieldName: "gender",
	formatAction: "capitalize"
},
{
	fieldName: "hair_color",
	formatAction: "capitalize"
},
{
	fieldName: "eye_color",
	formatAction: "capitalize"
},
{
	fieldName: "height",
	formatAction: "convert-to-feet-and-inches"
},
{
	fieldName: "mass",
	formatAction: "convert-to-pounds"
},
{
	fieldName: "birth_year",
	formatAction: null
}