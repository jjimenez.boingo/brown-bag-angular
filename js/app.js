var app = angular.module("BoingoApp", ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider){

	$urlRouterProvider.otherwise('/people');

	$stateProvider
	.state('people', {
		url: '/people',
		templateUrl: '../templates/people-template.html',
		controller: 'PeopleController'
	})
	.state('planets', {
		url: '/planets',
		templateUrl: '../templates/planets-template.html',
		controller: 'PlanetsController'
	})
	.state('starships', {
		url: '/starships',
		templateUrl: '../templates/starships-template.html',
		controller: 'StarshipsController'
	});
});

app.factory('StarWarsDataFactory', ['$http', '$q', function($http, $q){
	var dataFactory = {};

		var StarWarsAPIEndpoint = "http://swapi.co/api/";

		dataFactory.fetchStarWarsData = function(resourceName){
			return $http.get(StarWarsAPIEndpoint + resourceName)
			.then(function(successResponse){
				var responseData = successResponse.data;
				return responseData.results;
			})
			.catch(function(errorResponse){
				console.log('An error occurred: ' + errorResponse);
			});
		};

		return dataFactory;
}]);

app.controller("NavigationController", ['$scope', function($scope){
	$scope.pageTitle = "AngularJS Brown Bag";
}]);



app.value('PeopleInterestingFields', [
	{
		fieldName: "name",
		friendlyName: "Name",
		formatAction: null
	},
	{
		fieldName: "gender",
		friendlyName: "Gender",
		formatAction: "capitalize"
	},
	{
		fieldName: "hair_color",
		formatAction: "capitalize",
		friendlyName: "Hair Color"
	},
	{
		fieldName: "eye_color",
		formatAction: "capitalize",
		friendlyName: "Eye Color"
	},
	{
		fieldName: "height",
		formatAction: "convert-to-feet-and-inches",
		friendlyName: "Height"
	},
	{
		fieldName: "mass",
		formatAction: "convert-to-pounds",
		friendlyName: "Weight: "
	},
	{
		fieldName: "birth_year",
		formatAction: null,
		friendlyName: "Year Born: "
	}
]);

app.directive("starWarsCharacter", function(){
	return {
		restrict: 'E',
		templateUrl: 'templates/star-wars-character.html'
	}
});

app.controller("PeopleController", ['$scope', 'WordCaseService', 'PeopleInterestingFields', 'StarWarsDataFactory', function($scope, WordCaseService, PeopleInterestingFields, StarWarsDataFactory){
	

	$scope.title = WordCaseService.toAllCaps("People of Star Wars");

	$scope.subtitle = WordCaseService.capitalizeFirstLetter("who's who in the world of star wars!");

	StarWarsDataFactory.fetchStarWarsData('people/')
	.then(function(fetchedData){
		$scope.people = [];

		_.each(fetchedData, function(data, k){
			data.gender = WordCaseService.capitalize(data.gender);
			data.hair_color = WordCaseService.capitalize(data.hair_color);
			data.eye_color = WordCaseService.capitalize(data.eye_color);
			data.height = WordCaseService.convertToFeetAndInches(data.height);;
			data.mass = WordCaseService.convertToPounds(data.mass);
			$scope.people.push(data);
		});

	});

	$scope.Math = window.Math;
	var brk = null;
}]);

app.controller('PlanetsController', ['$scope', function($scope){
	$scope.title = "Planets of Star Wars";
	$scope.subtitle = "All the planets in the Star Wars Universe!"
}]);

app.controller('StarshipsController', ['$scope', function($scope){
	$scope.title = "Starships of Star Wars";
	$scope.subtitle = "All the starships in the Star Wars Universe!"
}]);