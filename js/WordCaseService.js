app.service("WordCaseService", function(){

	var sentenceWords = [];
	var allCapsWords = [];
	var capitalizedWords = [];

	var sentenceToArray = function(sentence){
		return sentence.split(' ');
	}

	var arrayToSentence = function(wordArray){
		return _.reduce(wordArray, function(memo, word){
			return memo + " " + word;
		});
	}

	this.toAllCaps = function(sentence){
		sentenceWords = sentenceToArray(sentence);

		allCapsWords = _.map(sentenceWords, function(word){
			return word.toUpperCase();
		});

		return arrayToSentence(allCapsWords);
	};

	this.capitalizeFirstLetter = function(sentence){
		sentenceWords = sentenceToArray(sentence);

		capitalizedWords = _.map(sentenceWords, function(word){
			return word.charAt(0).toUpperCase() + word.substring(1);
		});

		return arrayToSentence(capitalizedWords);
	};

	this.capitalize = function(fieldValue){
		return fieldValue.charAt(0).toUpperCase() + fieldValue.substr(1).toLowerCase();
	};

	this.convertToFeetAndInches= function(fieldValue){
		var metricValue = Number(fieldValue);

		var totalInchesFromCentimeters = Math.round(metricValue * 0.393701);


		var feet = Math.floor(totalInchesFromCentimeters/12);
		var inches = totalInchesFromCentimeters % 12;

		return feet + " Feet " + inches + " Inches"; 
	};

	this.convertToPounds = function(fieldValue){
		return Math.round(Number(fieldValue) * 2.20462) + " Pounds";
	};
	
});